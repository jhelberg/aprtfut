#+TITLE: Activate Pro Rate Term
#+OPTIONS: H:6 num:t toc:4 date:t \n:nil @:t ::t |:t ^:nil -:t f:t *:t <:t author:t creator:nil timestamp:t
#+HTML_HEAD: <meta name="keywords" content="woocommerce, woocommerce subscriptions"/>
#+LATEX_COMPILER: pdflatex
#+LATEX_HEADER: \usepackage{charter}\usepackage{svg}\usepackage{wasysym}
#+LATEX_HEADER: \setlength{\parskip}{1em}\setlength{\parindent}{0pt}
#+LATEX_HEADER: \DeclareUnicodeCharacter{25AE}{\ensuremath{\blacksquare}}
#+LATEX_HEADER: \DeclareUnicodeCharacter{2082}{\ensuremath{_2}}
#+LATEX_HEADER: \DeclareUnicodeCharacter{2083}{\ensuremath{_3}}
#+LATEX_HEADER: \DeclareUnicodeCharacter{2084}{\ensuremath{_4}}
#+LATEX_HEADER: \DeclareUnicodeCharacter{2085}{\ensuremath{_5}}
#+LATEX_HEADER: \DeclareUnicodeCharacter{2086}{\ensuremath{_6}}
#+LATEX_HEADER: \DeclareUnicodeCharacter{2087}{\ensuremath{_7}}
#+LATEX_HEADER: \DeclareUnicodeCharacter{2088}{\ensuremath{_8}}
#+LATEX_HEADER: \DeclareUnicodeCharacter{2089}{\ensuremath{_9}}
#+LATEX_HEADER: \DeclareUnicodeCharacter{2642}{\male}
#+LATEX_HEADER: \DeclareUnicodeCharacter{2640}{\female}
#+LANGUAGE: nl

* Subscriptions
  At [[https://www.gerrardstreet.nl][Gerrard Street]] we deliver
  high-end audio headphones on a subscription basis. None of the
  e-commerce applications fit our bill, Woocommerce fits
  least-worst. Our difference from other hardware sellers is that we
  rent out headphones. Our difference with other e-commerce rental
  businesses is that we rent out hardware.

  But apart from that, Woocommerce with /subscriptions/ is quite
  useful.

** Upgrades and moving the next payment date backward
  But then we started offering more than one product. People
  downgrading have their new subscription start-date moved away to
  cater for the amount of money paid too much in the running period
  (month or year). Much to our surprise, upgrading worked out quite
  differently. Next to a higher periodical fee, people were charged
  extra once for now until the end of the running period, because they
  paid for that period, but a little less than necessary to cater for
  the more expensive hardware. The code for pulling the start of the
  period forward is in /subscriptions/, but a not-set filter was
  tested before deciding to go for adding weird, hard to explain,
  amounts to the bill.

  This plugin is as simple as a plugin can be; it adds a function to
  the filter used in /subscriptions/ which returns =true= in all
  cases. This activates pro rate term for upgrades, like it already
  did for downgrades.

  To install, download the zip-file and go to the plugin-menu in your
  wordpress UI. Somewhere it says ‘New plugin’, activate it and choose
  ‘upload file’ or something similar. You can now upload the zip-file
  you just fetched from my source repository in order to install the
  plugin. 

*** the code for activating moving pay-dates backward
  :PROPERTIES:
  :ID:       4ae2615f-a980-4586-a378-c0c781c2ec97
  :END:
      #+begin_src php :tangle aprtfut.php
      <?php
      /**
       * Plugin Name: activate pro rate term for upgrades too
       * Plugin URI: https://www.gitlab.com/jhelberg/aprtfut
       * Description: The very first plugin that I have ever created.
       * Version: 1.1
       * Author: Joost Helberg
       * Author URI: https://www.gerrardstreet.nl
       */
     
      add_filter( 'wcs_switch_proration_reduce_pre_paid_term', 'gs_doaprtfut', 1, 7 );
      function gs_doaprtfut( $one, $two, $three, $four, $five ) {
      /*
        $handle = @fopen( "http://htip.helberg.nl/" . "aprtfut"        . "/" .
                                                      strval( $one )   . "/" . 
                                                      strval( $two )   . "/" .  
                                                      strval( $three ) . "/" .  
                                                      strval( $four )  . "/" .  
                                                      strval( $five )  . "/" .  
                                                      strval( $six )   . "/" .  
                                                      strval( $seven ) . "", "r" );
        if( $handle ) {
           @fclose( $handle );
        } */
        return true;
      }
      #+end_src

*** but that didn't quite work
   The code in class-wc-subscription-switcher.php doesn't work in case
   of upgrades. This code is not activated by default, it's probably
   never tested.

   The old code:
   #+begin_src php :tangle no
   if ( $reduce_pre_paid_term ) {
     $pre_paid_days = self::calculate_pre_paid_days( $old_recurring_total, $new_price_per_day );
     if ( $days_since_last_payment < $pre_paid_days ) {
       WC()->cart->cart_contents[ $cart_item_key ]['subscription_switch']['first_payment_timestamp'] = 
                $last_order_time_created + ( $pre_paid_days * 60 * 60 * 24 );
     } else {
       WC()->cart->cart_contents[ $cart_item_key ]['subscription_switch']['first_payment_timestamp'] = 
                0;
     }
   #+end_src

   The working code is (somewhere around line 1443):
   #+begin_src php :tangle no
   if ( $reduce_pre_paid_term ) {
       $new_period_first = ceil( ($old_recurring_total - ($old_price_per_day * $days_since_last_payment)) / $new_price_per_day );
       WC()->cart->cart_contents[ $cart_item_key ]['subscription_switch']['first_payment_timestamp'] = 
            $last_order_time_created + ( $days_since_last_payment * 60 * 60 * 24 ) + ( $new_period_first * 60 * 60 * 24 );
     }
   #+end_src

** coupons and upgrading
   :PROPERTIES:
   :ID:       1d3947fa-f5e1-4a0e-957d-97469a03ffdb
   :END:
   For customers with coupons the wrong new price per day is
   calculated. This is because the coupon is subtracted after all
   calculations, not while doing the calculations. These customers
   end up with the wrong next payment date.

   A filter is used for adapting the new price per day, we use that
   by moving the credits from the coupon into the price per day:

   #+begin_src php :tangle no
   $new_price_per_day = apply_filters( 'wcs_switch_proration_new_price_per_day', $new_price_per_day, $subscription, $cart_item, $days_in_new_cycle );
   #+end_src

   We can create a filter for handling special subscriptions where we
   reduce the new price per day to the one valid after applying the
   coupon. There is no coupon info though, so we need to have a list
   of subscription id's with coupon-info for adapting the price.

   #+begin_src php :tangle gsswitchcpns.php
     <?php
         /**
          ,* Plugin Name: recalculate new price per day taking into account coupons
          ,* Plugin URI: https://www.gitlab.com/jhelberg/aprtfut
          ,* Description: The second plugin that I have ever created.
          ,* Version: 1.1
          ,* Author: Joost Helberg
          ,* Author URI: https://www.gerrardstreet.nl
          ,*/
     add_filter( 'wcs_switch_proration_new_price_per_day', 'gs_doapplycoupons', 1, 4 );
     function gs_doapplycoupons( $newppd, $subsc, $item, $days_in_new_cycle ) {
        $twentypercents = array(
           45442,
           41993,
           41147,
           39098,
           37766,
           35777,
           33759,
           33730,
           33523,
           33521,
           33477,
           33304,
           32279,
           30210,
           30227,
           30221,
           30218,
           30219,
           30212,
           30138,
           29984,
           29970,
           29800,
           29444,
           29434,
           29415,
           29355,
           29313,
           29301,
           29276,
           29232,
           29171,
           29167,
           29162,
           29158,
           29140,
           29138,
           29133,
           29128,
           29122,
           29118,
           29113,
           29110,
           29103,
           29099,
           29094,
           29087,
           29084,
           29079,
           29075,
           29073,
           28289,
           28049,
           27916,
           29146,
           27221,
           27198,
           27153,
           27126,
           26900,
           26849,
           26839,
           26835,
           26794,
           26765,
           26734,
           26730,
           26681,
           26676,
           26658,
           26551,
           26522,
           31349,
           26496,
           26412,
           26355,
           26344,
           26342,
           26123,
           25989,
           25952,
           25936,
           25933,
           25902,
           25868,
           25865,
           25857,
           25812,
           25809,
           25800,
           25784,
           25753,
           25729,
           25655,
           25469,
           25301,
           25289,
           25263,
           25241,
           25159,
           25143,
           25109,
           25087,
           25041,
           24971,
           24963,
           24951,
           24907,
           24833,
           24799,
           24787,
           24781,
           24747,
           24743,
           24739,
           24719,
           24531,
           24485,
           24473,
           24425,
           24399,
           24315,
           23723,
           23545,
           23409,
           23193,
           23187,
           23179,
           22949,
           22897,
           22783,
           21459,
           21297,
           21287,
           21279,
           21223,
           21167,
           21137,
           21001,
           20975,
           20933,
           20905,
           20885,
           20859,
           20747,
           20581,
           20329,
           19723,
           19693,
           19665,
           19501,
           19481,
           19445,
           19041,
           18685,
           18571,
           18569,
           18563,
           19367,
           17241,
           17233,
           17161,
           17137,
           17129,
           14383,
           14329,
           14307,
           14299,
           14261,
           14243,
           14189,
           14153,
           14141,
           14125,
           13933,
           13861,
           13845,
           13797,
           13781,
           13733,
           13693,
           13677,
           13637,
           13629,
           13581,
           13517,
           13509,
           13469,
           13285,
           13257,
           13145,
           13017,
           13001,
           17045,
           16909,
           16861,
           16837,
           16829,
           16789,
           16781,
           16757,
           16749,
           16701,
           16645,
           16629,
           16621,
           16605,
           16581,
           16557,
           16533,
           16501,
           16445,
           16429,
           16413,
           16405,
           16389,
           16299,
           16275,
           16249,
           16241,
           16123,
           16115,
           16011,
           15987,
           15977,
           15953,
           15909,
           15877,
           15853,
           15813,
           15789,
           15765,
           15749,
           15725,
           15645,
           15589,
           15581,
           15525,
           15485,
           15453,
           15445,
           15437,
           15429,
           15405,
           15397,
           15389,
           15333,
           15315,
           15291,
           15227,
           15207,
           15159,
           15151,
           15047,
           15031,
           15023,
           15015,
           14985,
           14969,
           14955,
           14931,
           18257,
           14863,
           14827,
           14755,
           14747,
           14715,
           14701,
           14677,
           14669,
           14453, 
           47292 // this one is in staging
           );
        $itsaboss = $newppd > 0.26;
        $itsaday  = $newppd < 0.26;
        $theid = $subsc->get_id();
        if( $itsaboss && in_array( $theid, $twentypercents ) ) {
           return $newppd * 0.8;
        }
        return $newppd;
     }
   #+end_src


* create installable archive and publish README.org
  #+begin_src sh :exports code
   [ aprtfut.org -nt README.org ] && cp aprtfut.org README.org && git add README.org && git commit -m 'new README'
   zip aprtfut.zip aprtfut.php
   zip gsswitchcpns.zip gsswitchcpns.php
  #+end_src

  #+RESULTS:
  | [master   | e235336]         | new       | README |                |   |              |
  | 1         | file             | changed,  |      5 | insertions(+), | 5 | deletions(-) |
  | updating: | aprtfut.php      | (deflated |   62%) |                |   |              |
  | updating: | gsswitchcpns.php | (deflated |   41%) |                |   |              |
